'''TensorFlow implementation of http://arxiv.org/pdf/1312.6114v10.pdf'''

from __future__ import absolute_import, division, print_function

import math
import os 
import numpy as np
import scipy.misc
import pdb
import tensorflow as tf
from tensorflow.contrib import layers
from tensorflow.contrib import losses
from tensorflow.contrib.framework import arg_scope
from tensorflow.contrib.tensorboard.plugins import projector
import cv2
from scipy.misc import imsave
# from tensorflow.examples.tutorials.mnist import input_data
#from cifar_dataset import cifar

from progressbar import ETA, Bar, Percentage, ProgressBar

from vae import VAE
# from gan import GAN

flags = tf.flags
logging = tf.logging

flags.DEFINE_integer("batch_size", 183, "batch size")
flags.DEFINE_integer("updates_per_epoch", 1, "number of updates per epoch")
flags.DEFINE_integer("max_epoch", 200, "max epoch")
flags.DEFINE_float("learning_rate", 1e-3, "learning rate")
flags.DEFINE_string("working_directory", "./vae", "./vae")
flags.DEFINE_integer("hidden_size", 3, "size of the hidden VAE unit")
flags.DEFINE_string("model", "vae", "gan or vae")

FLAGS = flags.FLAGS

def iterate_minibatches(inputs,  batchsize = 128, shuffle=True):
   
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt]


if __name__ == "__main__":
    ## load lego data
    input_folder= "../../square/"

    import fnmatch
    import os

    files = []
    for root, dirnames, filenames in os.walk(input_folder):
        for filename in fnmatch.filter(filenames, '*.png'):
            files.append(os.path.join(root, filename))

    print("Loading LEGO data...")
    lego_data = np.empty((len(files), 64, 64, 3), dtype=np.float32)
    for idx, filename in enumerate(files):
        img = cv2.imread(filename)
        img = cv2.resize(img, (64, 64))
        lego_data[idx, ...] = img
    lego_data = (lego_data.reshape((-1, 64*64*3)) / 255) * 2 - 1
    print(lego_data.dtype, lego_data.max(), lego_data.min())

    data_directory = os.path.join(FLAGS.working_directory, "MNIST")
    if not os.path.exists(data_directory):
        os.makedirs(data_directory)
    # mnist = input_data.read_data_sets(data_directory, one_hot=True)

    assert FLAGS.model in ['vae', 'gan']
    if FLAGS.model == 'vae':
        model = VAE(FLAGS.hidden_size, FLAGS.batch_size, FLAGS.learning_rate)
    elif FLAGS.model == 'gan':
        raise ValueError("gan not accepted")
        # model = GAN(FLAGS.hidden_size, FLAGS.batch_size, FLAGS.learning_rate)
    
    size_sprite = 32
    config = projector.ProjectorConfig()
    embedding = config.embeddings.add()
    embedding.tensor_name = model.lego_embeddings.name
    embedding.sprite.image_path = "/home/guille/trash/clustering/sprites.png"
    # Specify the width and height of a single thumbnail.
    embedding.sprite.single_image_dim.extend([size_sprite, size_sprite])
    
    dataset=np.load('../../unsupervised_data_eroded.npy').astype('float32')
    dataset=np.transpose(dataset,(0,2,3,1)).reshape(-1, 64*64*3)
    print(dataset.max(), dataset.min())
    # dataset=dataset*0.5+0.5
    for epoch in range(FLAGS.max_epoch):
        training_loss = 0.0

        #pbar = ProgressBar()
        for images in (iterate_minibatches(dataset,batchsize = FLAGS.batch_size, shuffle=True)):
     #       images = iterate_minibatches(dataset,batchsize = FLAGS.batch_size, shuffle=True)
           # pdb.set_trace()
            loss_value = model.update_params(images)
            training_loss += loss_value

        training_loss = training_loss #/ \
            # (FLAGS.updates_per_epoch * FLAGS.batch_size)

        print("Loss %f" % training_loss)

        if ((epoch + 1) % 10) == 0:
            np_lego_embeddings = np.empty((len(files), FLAGS.hidden_size), dtype=np.float32)
            n_samples = len(files)
            num_batches = (n_samples + FLAGS.batch_size - 1) // FLAGS.batch_size
            for idx in range(num_batches):
                begin = idx*FLAGS.batch_size
                end = (idx + 1) * FLAGS.batch_size
                end = end if end <= n_samples else None
                batch = lego_data[begin:end, :]
                np_lego_embeddings[begin:end, :] = model.sess.run(model.embeddings, feed_dict={model.input_tensor: batch})
            model.sess.run(model.lego_emb_initializer,
                           feed_dict={model.pl_lego_embeddings: np_lego_embeddings})
            summary_writer = tf.summary.FileWriter(FLAGS.working_directory+"/epoch_"+str(epoch))
            # The next line writes a projector_config.pbtxt in the LOG_DIR. TensorBoard will
            # read this file during startup.
            projector.visualize_embeddings(summary_writer, config)
            summary_writer.close()

            model.generate_and_save_images(
                FLAGS.batch_size, FLAGS.working_directory+"/epoch_"+str(epoch),images)
            save_path = model.saver.save(model.sess, FLAGS.working_directory+"/epoch_"+str(epoch)+"/model.ckpt")
            print("Model saved in file: %s" % save_path)
    save_path = model.saver.save(model.sess, "./checkpoint/model.ckpt")

