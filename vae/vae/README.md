This code is a modified version of the code in this repository:
https://github.com/ikostrikov/TensorFlow-VAE-GAN-DRAW

Here, only the VAE component is used and has been adapted for RGB images of 64x64.
Tensorboard Embedding Projector is used for visualizing.
