import math
import os 
import numpy as np
import scipy.misc
import pdb
import tensorflow as tf
from tensorflow.contrib import layers
from vae import VAE
#~ flags = tf.flags
#~ flags.DEFINE_integer("batch_size", 1, "batch size")
#~ flags.DEFINE_float("learning_rate", 1e-3, "learning rate")
#~ flags.DEFINE_integer("hidden_size", 3, "size of the hidden VAE unit")
#~ FLAGS = flags.FLAGS
g_2 = tf.Graph()
hidden_size = 3


with g_2.as_default():
    model = VAE(hidden_size, graph=g_2)
    model.load_checkpoint('checkpoint_vae/model.ckpt')

#~ mean,stddev,logvar = model.get_latent_vectors(img)
