import numpy as np
import cv2

input_folder= "square_balanced/"

import fnmatch
import os

files = []
for root, dirnames, filenames in os.walk(input_folder):
    for filename in fnmatch.filter(filenames, '*.png'):
        files.append(os.path.join(root, filename))

print(len(files))

data = []
kernel = np.ones((5, 5), np.uint8)
for file in files:
    img = cv2.imread(file)
    img = cv2.resize(img, (64, 64))
    eroded_img = cv2.erode(img, kernel, iterations=1)

    data.append(eroded_img)

np.save("unsupervised_data_eroded.npy", ((np.asarray(data).astype(np.float32)/255 - 0.5) * 2).transpose(0,3,1,2))
